/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : apimgr

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2017-09-19 13:50:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for api_content
-- ----------------------------
DROP TABLE IF EXISTS `api_content`;
CREATE TABLE `api_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'api接口编号',
  `api_name` varchar(255) NOT NULL COMMENT 'api接口名称',
  `api_desc` varchar(255) NOT NULL COMMENT 'api接口描述',
  `request_url` varchar(255) NOT NULL COMMENT 'api接口请求的url',
  `request_method` varchar(255) NOT NULL COMMENT 'api接口请求的方式',
  `request_head` varchar(255) NOT NULL COMMENT 'api接口请求头',
  `request_param` varchar(255) NOT NULL COMMENT 'api接口请求参数字符串',
  `project_id` int(11) NOT NULL COMMENT '项目编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='api内容';

-- ----------------------------
-- Table structure for api_content_type
-- ----------------------------
DROP TABLE IF EXISTS `api_content_type`;
CREATE TABLE `api_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type_name` varchar(255) NOT NULL COMMENT '类型名称',
  `parent_id` int(11) NOT NULL COMMENT '父编号',
  `project_id` int(11) NOT NULL COMMENT '项目编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='api接口分类表';

-- ----------------------------
-- Table structure for api_project
-- ----------------------------
DROP TABLE IF EXISTS `api_project`;
CREATE TABLE `api_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(255) NOT NULL COMMENT '项目名称',
  `user_id` int(11) NOT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='api项目';

-- ----------------------------
-- Table structure for api_request_param_desc
-- ----------------------------
DROP TABLE IF EXISTS `api_request_param_desc`;
CREATE TABLE `api_request_param_desc` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '请求参数描述编号',
  `request_param_name` varchar(255) NOT NULL COMMENT '请求参数名称',
  `is_required` tinyint(255) DEFAULT NULL COMMENT '是否必填',
  `data_type` varchar(255) DEFAULT NULL COMMENT '数据类型',
  `req_param_desc` varchar(255) DEFAULT NULL COMMENT '参数描述',
  `content_id` int(11) DEFAULT NULL COMMENT '所属接口编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='请求参数描述表';

-- ----------------------------
-- Table structure for api_response
-- ----------------------------
DROP TABLE IF EXISTS `api_response`;
CREATE TABLE `api_response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response_content` varchar(255) NOT NULL COMMENT '响应的内容',
  `response_type` int(255) NOT NULL COMMENT '响应的类型(1.正确响应   2.错误响应)',
  `api_content_id` int(255) NOT NULL COMMENT 'api接口内容编号',
  `wrong_enum` varchar(255) DEFAULT NULL COMMENT '异常枚举',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='api响应返回';

-- ----------------------------
-- Table structure for api_response_desc
-- ----------------------------
DROP TABLE IF EXISTS `api_response_desc`;
CREATE TABLE `api_response_desc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `param_name` varchar(255) NOT NULL COMMENT '响应参数名称',
  `param_type` varchar(255) NOT NULL COMMENT '响应参数数据类型',
  `param_desc` varchar(255) NOT NULL COMMENT '响应参数描述',
  `response_id` int(11) NOT NULL COMMENT '响应描述所属的响应编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='api响应参数描述';

-- ----------------------------
-- Table structure for api_user
-- ----------------------------
DROP TABLE IF EXISTS `api_user`;
CREATE TABLE `api_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='api用户';
