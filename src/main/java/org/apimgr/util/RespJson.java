package org.apimgr.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apimgr.enums.ResultStateEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * Created by yangjie on 2017/8/2.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RespJson implements Serializable{


    private static final Logger logger = LoggerFactory.getLogger(RespJson.class);

    /**
     * 响应代码
     */
    protected int resultCode;

    /**
     * 错误消息
     */
    protected String resultMsg;

    /**
     * 响应的数据域
     */
    protected Object data;


    /**
     * 构建成功响应
     */
    public static RespJson buildSuccessResponse() {
        return buildSuccessResponse(null);
    }

    /**
     * 构建成功响应
     *
     * @param data 返回的数据对象 (JSon数组 或 Json对象)
     */
    public static RespJson buildSuccessResponse(Object data) {
        RespJson respJson = new RespJson();
        respJson.setResultCode(ResultStateEnum.SUCCESS.getCode());
        respJson.setResultMsg(ResultStateEnum.SUCCESS.getDesc());
        respJson.setData(data);
        return respJson;
    }


    /**
     * 构建成功响应
     *
     * @param resultMsg 返回给前端的信息
     * @param data      返回的数据对象 (JSon数组 或 Json对象)
     */
    public static RespJson buildSuccessResponse(String resultMsg, Object data) {
        RespJson respJson = new RespJson();
        respJson.setResultCode(ResultStateEnum.SUCCESS.getCode());
        respJson.setResultMsg(resultMsg);
        respJson.setData(data);
        return respJson;
    }

    /**
     * 构建失败响应
     *
     * @param resultCode 响应代码
     * @param errorMsg   返回给前端的错误信息
     */
    public static RespJson buildFailureResponse(int resultCode, String errorMsg) {
        RespJson respJson = new RespJson();
        respJson.setResultCode(resultCode);
        respJson.setResultMsg(errorMsg);
        respJson.setData(null);
        return respJson;
    }

    /**
     * 构建失败响应
     *
     * @param errorMsg 返回给前端的错误信息
     */
    public static RespJson buildFailureResponse(String errorMsg) {
        return buildFailureResponse(ResultStateEnum.FAIL.getCode(), errorMsg);
    }

    /**
     * 获得 resultCode
     */
    public int getResultCode() {
        return resultCode;
    }

    /**
     * 设置 resultCode
     */
    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    /**
     * 获得 resultMsg
     */
    public String getResultMsg() {
        return resultMsg;
    }

    /**
     * 设置 resultMsg
     */
    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    /**
     * 获得 data
     */
    public Object getData() {
        return data;
    }

    /**
     * 设置 data
     */
    public void setData(Object data) {
        this.data = data;
    }
}
