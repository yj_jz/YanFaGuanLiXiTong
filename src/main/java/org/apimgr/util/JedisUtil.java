package org.apimgr.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Created by yangjie on 2017/8/2.
 * Jedis工具类
 */
@Component
public class JedisUtil {

    @Autowired
    private  StringRedisTemplate stringRedisTemplate;

    public  void set(String key,String value){
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        ops.set(key, value);
    }

    public String get(String key){
        return stringRedisTemplate.opsForValue().get(key);
    }

    public void del(String key){
        this.stringRedisTemplate.delete(key);
    }

    public  void  expilre(String key,long min){
        String value = stringRedisTemplate.opsForValue().get(key);
        stringRedisTemplate.opsForValue().set(key, value,min, TimeUnit.MINUTES);
    }

    public  boolean exists(String key){
        return  stringRedisTemplate.hasKey(key);
    }


}
