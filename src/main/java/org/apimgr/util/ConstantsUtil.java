package org.apimgr.util;

/**
 * Created by yangjie on 2017/9/7.
 * 字符串常量util
 */
public class ConstantsUtil {

    //正确响应编码
    public  static  final  Integer RIGHT_RESPONSE_CODE = 1;

    //错误响应编码
    public  static  final  Integer WRONG_RESPONSE_CODE = 2;

    //bootstrap.css的Url
    public  static  final  String BOOTSTRAP_CSS_URL = "https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css";

    //Jquery的url
    public  static  final  String JQUERY_URL = "https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js";

    //bootstrap.js的url
    public  static  final  String BOOTSTRAP_JS_URL = "https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js";

    //bootstrap.css导出后存放的路径
    public  static  final String EXPORT_BOOTSTRAP_CSS_PATH = "\\bootstrap\\bootstrap.css";

    //bootstrap.js导出后存放的路径
    public  static  final String EXPORT_BOOTSTRAP_JS_PATH = "\\bootstrap\\bootstrap.js";

    //jquery.js导出后存放的路径
    public  static  final String EXPORT_JQUERY_PATH = "\\bootstrap\\jquery.js";

    //api接口html导出后存放的路径
    public static  final  String EXPORT_API_PATH = "";
}
