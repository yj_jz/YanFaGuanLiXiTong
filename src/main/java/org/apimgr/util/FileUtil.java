package org.apimgr.util;

import javax.swing.filechooser.FileSystemView;

/**
 * Created by yangjie on 2017/9/7.
 */
public class FileUtil {

    /**
     * 获取桌面路径
     * @return
     */
    public static String getDeskTopPath() {
        String deskTopPath = FileSystemView.getFileSystemView().getHomeDirectory().getAbsolutePath();
        return  deskTopPath;
    }
}
