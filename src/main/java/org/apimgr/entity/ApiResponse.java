package org.apimgr.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * 响应结果类
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse implements Serializable {

    private static final long serialVersionUID = 2655835512636007168L;


    /**
     *
     */
    private Integer id;


    /**
     * 响应的内容
     */
    private String response_content;


    /**
     * 响应的类型(1.正确响应   2.错误响应)
     */
    private Integer response_type;


    /**
     * api接口内容编号
     */
    private Integer api_content_id;

    /**
     * 错误信息的枚举
     */
    private String wrong_enum;


    public Integer getId(){
        return id;
    }


    public void setId( Integer id){
        this.id=id;
    }


    public String getResponse_content(){
        return response_content;
    }


    public void setResponse_content( String response_content){
        this.response_content=response_content;
    }


    public Integer getResponse_type(){
        return response_type;
    }


    public void setResponse_type( Integer response_type){
        this.response_type=response_type;
    }


    public Integer getApi_content_id(){
        return api_content_id;
    }


    public void setApi_content_id( Integer api_content_id){
        this.api_content_id=api_content_id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getWrong_enum() {
        return wrong_enum;
    }

    public void setWrong_enum(String wrong_enum) {
        this.wrong_enum = wrong_enum;
    }
}