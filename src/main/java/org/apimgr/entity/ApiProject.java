package org.apimgr.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * 项目类
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiProject implements Serializable {

    private static final long serialVersionUID = -5428280621697017274L;


    /**
     *
     */
    private Integer id;


    /**
     * 项目名称
     */
    private String project_name;


    /**
     * 用户编号
     */
    private Integer user_id;


    public Integer getId(){
        return id;
    }


    public void setId( Integer id){
        this.id=id;
    }


    public String getProject_name(){
        return project_name;
    }


    public void setProject_name( String project_name){
        this.project_name=project_name;
    }


    public Integer getUser_id(){
        return user_id;
    }


    public void setUser_id( Integer user_id){
        this.user_id=user_id;
    }


}