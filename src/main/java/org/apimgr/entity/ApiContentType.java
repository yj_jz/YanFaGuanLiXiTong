package org.apimgr.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * api接口分类Bean
 * Create by yangjie on 2017/08/24
*/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiContentType implements Serializable{


	/**
	 * 编号
	 */
	private Integer id;


	/**
	 * 类型名称
	 */
	private String type_name;


	/**
	 * 父编号
	 */
	private Integer parent_id;


	/**
	 * 项目编号
	 */
	private Integer project_id;


	public Integer getId(){
		return id;
	}


	public void setId( Integer id){
		this.id=id;
	}


	public String getType_name(){
		return type_name;
	}


	public void setType_name( String type_name){
		this.type_name=type_name;
	}


	public Integer getParent_id(){
		return parent_id;
	}


	public void setParent_id( Integer parent_id){
		this.parent_id=parent_id;
	}


	public Integer getProject_id(){
		return project_id;
	}


	public void setProject_id( Integer project_id){
		this.project_id=project_id;
	}


}