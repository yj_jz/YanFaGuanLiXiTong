package org.apimgr.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * API接口内容
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiContent implements Serializable{

    private static final long serialVersionUID = 7177811169568226188L;

    /**
     * api接口编号
     */
    private Integer id;


    /**
     * api接口名称
     */
    private String api_name;


    /**
     * api接口描述
     */
    private String api_desc;


    /**
     * api接口请求的url
     */
    private String request_url;


    /**
     * api接口请求的方式
     */
    private String request_method;


    /**
     * api接口请求头
     */
    private String request_head;


    /**
     * api接口请求参数字符串
     */
    private String request_param;


    /**
     * 项目编号
     */
    private Integer project_id;


    public Integer getId(){
        return id;
    }


    public void setId( Integer id){
        this.id=id;
    }


    public String getApi_name(){
        return api_name;
    }


    public void setApi_name( String api_name){
        this.api_name=api_name;
    }


    public String getApi_desc(){
        return api_desc;
    }


    public void setApi_desc( String api_desc){
        this.api_desc=api_desc;
    }


    public String getRequest_url(){
        return request_url;
    }


    public void setRequest_url( String request_url){
        this.request_url=request_url;
    }


    public String getRequest_method(){
        return request_method;
    }


    public void setRequest_method( String request_method){
        this.request_method=request_method;
    }


    public String getRequest_head(){
        return request_head;
    }


    public void setRequest_head( String request_head){
        this.request_head=request_head;
    }


    public String getRequest_param(){
        return request_param;
    }


    public void setRequest_param( String request_param){
        this.request_param=request_param;
    }


    public Integer getProject_id(){
        return project_id;
    }


    public void setProject_id( Integer project_id){
        this.project_id=project_id;
    }


}