package org.apimgr.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Created by yangjie on 2017/9/7.
 * api响应参数描述Bean
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponseDesc implements Serializable{

    private static final long serialVersionUID = 373322803621162820L;


    /**
     *
     */
    private Integer id;


    /**
     * 响应参数名称
     */
    private String param_name;


    /**
     * 响应参数数据类型
     */
    private String param_type;


    /**
     * 响应参数描述
     */
    private String param_desc;


    /**
     * 响应描述所属的响应编号
     */
    private  Integer response_id;

    public Integer getId(){
        return id;
    }


    public void setId( Integer id){
        this.id=id;
    }


    public String getParam_name(){
        return param_name;
    }


    public void setParam_name( String param_name){
        this.param_name=param_name;
    }


    public String getParam_type(){
        return param_type;
    }


    public void setParam_type( String param_type){
        this.param_type=param_type;
    }


    public String getParam_desc(){
        return param_desc;
    }


    public void setParam_desc( String param_desc){
        this.param_desc=param_desc;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getResponse_id() {
        return response_id;
    }

    public void setResponse_id(Integer response_id) {
        this.response_id = response_id;
    }
}