package org.apimgr.mapper;

import org.apimgr.dto.ApiProjectReqDto;
import org.apimgr.entity.ApiProject;

import java.util.List;

/**
 * api项目Mapper 
 * Create by yangjie on 2017/09/06
*/
public interface ApiProjectMapper{

    /**
     * 多条件查询api项目
     * @param reqDto
     * @return
     */
	public List<ApiProject> getApiProjectByParams(ApiProjectReqDto reqDto);

    /**
     * 添加api项目
     * @param reqDto
     * @return
     */
	public int addApiProject(ApiProjectReqDto reqDto);

    /**
     * 修改api项目
     * @param reqDto
     * @return
     */
	public int updateApiProject(ApiProjectReqDto reqDto);

    /**
     * 删除api项目
     * @param reqDto
     * @return
     */
	public int deleteApiProject(ApiProjectReqDto reqDto);

}