package org.apimgr.mapper;

import org.apimgr.dto.ApiResponseReqDto;
import org.apimgr.entity.ApiResponse;

import java.util.List;

/**
 * api响应返回Mapper 
 * Create by yangjie on 2017/09/06
*/
public interface ApiResponseMapper{

    /**
     * 多条件查询api响应返回
     * @param reqDto
     * @return
     */
	public List<ApiResponse> getApiResponseByParams(ApiResponseReqDto reqDto);

    /**
     * 添加api响应返回
     * @param reqDto
     * @return
     */
	public int addApiResponse(ApiResponseReqDto reqDto);

    /**
     * 修改api响应返回
     * @param reqDto
     * @return
     */
	public int updateApiResponse(ApiResponseReqDto reqDto);

    /**
     * 删除api响应返回
     * @param reqDto
     * @return
     */
	public int deleteApiResponse(ApiResponseReqDto reqDto);


    /**
     * 批量插入api响应返回
     * @param reqDto
     * @return
     */
	public  int batchAddApiResponse(ApiResponseReqDto reqDto);

}