package org.apimgr.mapper;

import org.apimgr.dto.ApiUserReqDto;
import org.apimgr.entity.ApiUser;

public interface ApiUserMapper {


    /**
     * 获取登录用户
     * @param apiUserReqDto
     * @return
     */
    public ApiUser getLoginUser(ApiUserReqDto apiUserReqDto);
}