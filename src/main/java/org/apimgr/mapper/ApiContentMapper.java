package org.apimgr.mapper;

import org.apimgr.dto.ApiContentReqDto;
import org.apimgr.entity.ApiContent;

import java.util.List;

/**
 * api内容Mapper 
 * Create by yangjie on 2017/09/06
*/
public interface ApiContentMapper{

    /**
     * 多条件查询api内容
     * @param reqDto
     * @return
     */
	public List<ApiContent> getApiContentByParams(ApiContentReqDto reqDto);

    /**
     * 添加api内容
     * @param reqDto
     * @return
     */
	public int addApiContent(ApiContentReqDto reqDto);

    /**
     * 修改api内容
     * @param reqDto
     * @return
     */
	public int updateApiContent(ApiContentReqDto reqDto);

    /**
     * 删除api内容
     * @param reqDto
     * @return
     */
	public int deleteApiContent(ApiContentReqDto reqDto);

}