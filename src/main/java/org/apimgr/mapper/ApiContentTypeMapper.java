package org.apimgr.mapper;

import org.apimgr.dto.ApiContentTypeReqDto;
import org.apimgr.entity.ApiContentType;

import java.util.List;

/**
 * api接口分类Mapper 
 * Create by yangjie on 2017/09/06 
*/
public interface ApiContentTypeMapper{

    /**
     * 多条件查询api接口分类
     * @param reqDto
     * @return
     */
	public List<ApiContentType> getApiContentTypeByParams(ApiContentTypeReqDto reqDto);

    /**
     * 添加api接口分类
     * @param reqDto
     * @return
     */
	public int addApiContentType(ApiContentTypeReqDto reqDto);

    /**
     * 修改api接口分类
     * @param reqDto
     * @return
     */
	public int updateApiContentType(ApiContentTypeReqDto reqDto);

    /**
     * 删除api接口分类
     * @param reqDto
     * @return
     */
	public int deleteApiContentType(ApiContentTypeReqDto reqDto);

}