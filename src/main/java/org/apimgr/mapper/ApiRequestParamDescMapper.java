package org.apimgr.mapper;

import org.apache.ibatis.annotations.Param;
import org.apimgr.dto.ApiRequestParamDescReqDto;
import org.apimgr.entity.ApiRequestParamDesc;

import java.util.List;

/**
 * 请求参数描述Mapper
 * Create by yangjie on 2017/09/06
*/
public interface ApiRequestParamDescMapper{

    /**
     * 多条件查询请求参数描述
     * @return
     */
	public List<ApiRequestParamDesc> getApiRequestParamDescByParams(ApiRequestParamDescReqDto reqDto);

    /**
     * 添加请求参数描述
     * @param reqDto
     * @return
     */
	public int addApiRequestParamDesc(ApiRequestParamDescReqDto reqDto);

    /**
     * 修改请求参数描述
     * @param reqDto
     * @return
     */
	public int updateApiRequestParamDesc(ApiRequestParamDescReqDto reqDto);

    /**
     * 删除请求参数描述
     * @param reqDto
     * @return
     */
	public int deleteApiRequestParamDesc(ApiRequestParamDescReqDto reqDto);

    /**
     * 批量添加请求描述
     * @param contentId  所属接口编号
     * @param list
     * @return
     */
    public  int batchAddApiRequestParamDesc(@Param("contentId") Integer contentId, @Param("list") List<ApiRequestParamDesc> list);
}