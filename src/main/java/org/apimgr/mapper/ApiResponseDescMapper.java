package org.apimgr.mapper;

import org.apache.ibatis.annotations.Param;
import org.apimgr.dto.ApiResponseDescReqDto;
import org.apimgr.entity.ApiResponseDesc;

import java.util.List;

/**
 * api响应参数描述Mapper 
 * Create by yangjie on 2017/09/06
*/
public interface ApiResponseDescMapper{

    /**
     * 多条件查询api响应参数描述
     * @param reqDto
     * @return
     */
	public List<ApiResponseDesc> getApiResponseDescByParams(ApiResponseDescReqDto reqDto);

    /**
     * 添加api响应参数描述
     * @param reqDto
     * @return
     */
	public int addApiResponseDesc(ApiResponseDescReqDto reqDto);

    /**
     * 修改api响应参数描述
     * @param reqDto
     * @return
     */
	public int updateApiResponseDesc(ApiResponseDescReqDto reqDto);

    /**
     * 删除api响应参数描述
     * @param reqDto
     * @return
     */
	public int deleteApiResponseDesc(ApiResponseDescReqDto reqDto);


    /**
     * 批量插入api响应描述
     * @param reqDto
     * @param repsonse_id
     * @return
     */
	public  int batchAddApiResponseDesc(@Param("reqDto") List<ApiResponseDesc> reqDto,@Param("response_id") Integer repsonse_id);

    /**
     * 根据响应编号集合删除响应描述
     * @param responseIdList
     * @return
     */
    public int deleteResponseDescByIds(@Param("list") List<Integer> responseIdList);
}