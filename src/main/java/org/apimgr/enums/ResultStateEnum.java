package org.apimgr.enums;

/**
 * Created by yangjie on 2017/8/2.
 * 枚举返回状态
 */
public enum ResultStateEnum {

    FAIL(0,"失败"),
    SUCCESS(1, "成功"),
    LOGIN_TIMEOUT(2, "登录超时");


    private final  int code;

    private final String desc;

    private ResultStateEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
