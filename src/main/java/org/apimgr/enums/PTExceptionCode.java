package org.apimgr.enums;

/**
 * Created by yangjie on 2017/8/2.
 * 异常码枚举
 */
public enum PTExceptionCode {

    DB_OPERATOR_EXPT("0001", "数据库操作发生异常", "数据库操作发生异常"),

    BD_UPDATE_EXPT("0002", "UpdateException", "更新数据出错"),
    BD_INSERT_EXPT("0003","InsertException","插入数据出错"),
    BD_DELETE_EXPT("0004","DeleteException","删除数据出错"),

    //系统管理操作异常 （0100-1000）
    SYS_AUTH_TOKEN_ERROR("0100", "AuthTokenError", "鉴权失败,无法获取token!"),


    SYSUSER_USERNAME_NULL("1001", "请输入用户名", "请输入用户名"),

    SYSUSER_PASSWORD_NULL("1002", "请输入密码", "请输入密码"),

    SYSUSER_LOGIN_FAIL("1003", "用户名或密码错误", "用户名或密码错误");

    private String code;

    private String desin;

    private String desout;

    private PTExceptionCode(String code, String desin, String desout) {
        this.code = code;
        this.desin = desin;
        this.desout = desout;
    }

    public String getCode() {
        return code;
    }

    public String getDesin() {
        return desin;
    }

    public String getDesout() {
        return desout;
    }

    public static PTExceptionCode getByCode(String code) {
        if (code != null && !"".equals(code.trim())) {
            for (PTExceptionCode mnum : values()) {
                if (mnum.getCode().equals(code)) {
                    return mnum;
                }
            }
        }
        return null;
    }

    public static String getInMsg(String code) {
        if (code != null && !"".equals(code.trim())) {
            for (PTExceptionCode mnum : values()) {
                if (mnum.getCode().equals(code)) {
                    return mnum.getDesin();
                }
            }
        }
        return null;
    }

    public static String getOutMsg(String code) {
        if (code != null && !"".equals(code.trim())) {
            for (PTExceptionCode mnum : values()) {
                if (mnum.getCode().equals(code)) {
                    return mnum.getDesout();
                }
            }
        }
        return null;
    }

    /**
     * 根据异常的枚举值返回提示给客户端的异常信息内容
     *
     * @param PTExceptionCode 异常的枚举值
     * @return 提示给客户端的异常信息内容
     */
    public static String getExceptionMsg(PTExceptionCode PTExceptionCode) {
        return ("[" + PTExceptionCode.getCode() + "]" + PTExceptionCode.getDesout());
    }

}
