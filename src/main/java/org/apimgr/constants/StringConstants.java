package org.apimgr.constants;

/**
 * Created by yangjie on 2017/8/2.
 */
public class StringConstants {

    public static final String SYSTEM_ERROR = "系统处理异常!";

    public static final String EMPTY_STRING = "";

    public static final String DEFAULT_STRING_SEPARATOR = ",";

    public static final String TOKEN = "token";
}
