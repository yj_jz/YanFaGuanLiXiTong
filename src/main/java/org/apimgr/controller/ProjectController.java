package org.apimgr.controller;

import com.alibaba.fastjson.JSON;
import org.apimgr.annotation.AuthReq;
import org.apimgr.constants.StringConstants;
import org.apimgr.dto.ApiContentResDto;
import org.apimgr.dto.ApiProjectReqDto;
import org.apimgr.dto.ApiProjectResDto;
import org.apimgr.exception.PTUnCheckedException;
import org.apimgr.service.ApiProjectService;
import org.apimgr.util.RespJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by yangjie on 2017/8/2.
 */
@RestController
@RequestMapping("/project")
public class ProjectController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource
    private ApiProjectService apiProjectService;

    /**
     * 多条件查询Api项目信息
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/getapiprojectbyparam")
    @AuthReq
    public RespJson getApiProjectByParam(@RequestBody ApiProjectReqDto reqDto, String token){
        logger.info("多条件查询Api项目信息"+ JSON.toJSONString(reqDto)+",token="+token);
        ApiProjectResDto resDto = null;
        try {
            resDto = apiProjectService.getApiProjectByParam(reqDto,token);
        }catch (PTUnCheckedException e){
            return  RespJson.buildFailureResponse(e.getOutMsg());
        }catch (Exception e){
            e.printStackTrace();
            return  RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse(resDto.getApiProjectList());
    }


    /**
     * 添加API项目信息
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/addapiproject")
    @AuthReq
    public RespJson addApiProject(@RequestBody ApiProjectReqDto reqDto,String token){
        logger.info("添加Api项目信息"+ JSON.toJSONString(reqDto)+",token="+token);
        ApiProjectResDto resDto = null;
        try {
            resDto = apiProjectService.addApiProject(reqDto);
        }catch (PTUnCheckedException e){
            return  RespJson.buildFailureResponse(e.getOutMsg());
        }catch (Exception e){
            e.printStackTrace();
            return  RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }


    /**
     * 修改API项目信息
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/updateapiproject")
    @AuthReq
    public RespJson updateApiProject(@RequestBody ApiProjectReqDto reqDto,String token){
        logger.info("修改Api项目信息"+ JSON.toJSONString(reqDto)+",token="+token);
        ApiProjectResDto resDto = null;
        try {
            resDto = apiProjectService.updateApiProject(reqDto);
        }catch (PTUnCheckedException e){
            return  RespJson.buildFailureResponse(e.getOutMsg());
        }catch (Exception e){
            e.printStackTrace();
            return  RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }


    /**
     * 删除Api项目信息
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/deleteapiproject")
    @AuthReq
    public RespJson deleteapiproject(@RequestBody ApiProjectReqDto reqDto,String token){
        logger.info("删除Api项目信息"+ JSON.toJSONString(reqDto)+",token="+token);
        ApiProjectResDto resDto = null;
        try {
            resDto = apiProjectService.deleteApiProject(reqDto);
        }catch (PTUnCheckedException e){
            return  RespJson.buildFailureResponse(e.getOutMsg());
        }catch (Exception e){
            e.printStackTrace();
            return  RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }


    /**
     * 将接口列表导出成Html
     * @param reqDto
     * @param token
     * @param response
     * @return
     */
    @RequestMapping("/exporthtml")
    @AuthReq
    public RespJson exportHtml(@RequestBody ApiProjectReqDto reqDto, String token, HttpServletRequest request, HttpServletResponse response) {
        logger.info("将接口列表导出成Html" + JSON.toJSONString(reqDto).toString());
        ApiContentResDto resDto = null;
        try {
            apiProjectService.exportHtml(reqDto,request,response);
        } catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        } catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }

}
