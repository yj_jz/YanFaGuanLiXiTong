package org.apimgr.controller;

import com.alibaba.fastjson.JSON;
import org.apimgr.annotation.AuthReq;
import org.apimgr.constants.StringConstants;
import org.apimgr.dto.ApiContentReqDto;
import org.apimgr.dto.ApiContentResDto;
import org.apimgr.exception.PTUnCheckedException;
import org.apimgr.service.ApiContentService;
import org.apimgr.util.RespJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by yangjie on 2017/8/2.
 */
@RestController
@RequestMapping("/content")
public class ContentController {

    private static final Logger logger = LoggerFactory.getLogger(ContentController.class);

    @Resource
    private ApiContentService apiContentService;

    /**
     * 多条件查询api内容
     *
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/getapicontentbyparams")
    @AuthReq
    public RespJson getApiContentByParams(@RequestBody ApiContentReqDto reqDto, String token) {
        logger.info(" 多条件查询api内容" + JSON.toJSONString(reqDto).toString());
        ApiContentResDto resDto = null;
        try {
            resDto = apiContentService.getApiContentByParam(reqDto);
        } catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        } catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }

        return RespJson.buildSuccessResponse(resDto.getApiContentList());
    }

    /**
     * 添加api内容
     *
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/addapicontent")
    @AuthReq
    public RespJson addApiContent(@RequestBody ApiContentReqDto reqDto,String token) {
        logger.info(" 添加api内容" + JSON.toJSONString(reqDto).toString());
        ApiContentResDto resDto = null;
        try {
            resDto = apiContentService.addApiContent(reqDto);
        } catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        } catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }

    /**
     * 修改api内容
     *
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/updateapicontent")
    @AuthReq
    public RespJson updateApiContent(@RequestBody ApiContentReqDto reqDto, String token) {
        logger.info(" 修改api内容" + JSON.toJSONString(reqDto).toString());
        ApiContentResDto resDto = null;
        try {
            resDto = apiContentService.updateApiContent(reqDto);
        } catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        } catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }

    /**
     * 删除api内容
     *
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/deleteapicontent")
    @AuthReq
    public RespJson deleteApiContent(@RequestBody ApiContentReqDto reqDto, String token) {
        logger.info(" 删除api内容" + JSON.toJSONString(reqDto).toString());
        ApiContentResDto resDto = null;
        try {
            resDto = apiContentService.deleteApiContent(reqDto);
        } catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        } catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }



}
