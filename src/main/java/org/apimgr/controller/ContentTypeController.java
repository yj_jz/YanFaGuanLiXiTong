package org.apimgr.controller;

import com.alibaba.fastjson.JSON;
import org.apimgr.annotation.AuthReq;
import org.apimgr.constants.StringConstants;
import org.apimgr.dto.ApiContentTypeReqDto;
import org.apimgr.dto.ApiContentTypeResDto;
import org.apimgr.exception.PTUnCheckedException;
import org.apimgr.service.ApiContentTypeService;
import org.apimgr.util.RespJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * api接口分类Controller
 * Create by yangjie on 2017/08/24
 */
@RestController
@RequestMapping("/apicontenttype")
public class ContentTypeController {

	//日志
	private static final Logger logger = LoggerFactory.getLogger(ContentTypeController.class);

	@Resource
	private ApiContentTypeService apiContentTypeService;

	/**
	 * 多条件查询api接口分类
	 * @param reqDto
	 * @param token
	 * @return
	 */
	@RequestMapping("/getapicontenttypebyparams")
	@AuthReq
	public RespJson getApiContentTypeByParams(@RequestBody ApiContentTypeReqDto reqDto, String token){
		logger.info(" 多条件查询api接口分类"+ JSON.toJSONString(reqDto));
		ApiContentTypeResDto resDto = null;
		try{
			resDto = apiContentTypeService.getApiContentTypeByParams(reqDto);
		}catch (PTUnCheckedException e) {
			return RespJson.buildFailureResponse(e.getOutMsg());
		}catch (Exception e) {
			e.printStackTrace();
			return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
		}
		return RespJson.buildSuccessResponse(resDto);
	}

	/**
	 * 添加api接口分类
	 * @param reqDto
	 * @param token
	 * @return
	 */
	@RequestMapping("/addapicontenttype")
	@AuthReq
	public RespJson addApiContentType(@RequestBody ApiContentTypeReqDto reqDto,String token){
		logger.info(" 添加api接口分类"+ JSON.toJSONString(reqDto));
		ApiContentTypeResDto resDto = null;
		try{
			resDto = apiContentTypeService.addApiContentType(reqDto);
		}catch (PTUnCheckedException e) {
			return RespJson.buildFailureResponse(e.getOutMsg());
		}catch (Exception e) {
			e.printStackTrace();
			return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
		}
		return RespJson.buildSuccessResponse();
	}

	/**
	 * 修改api接口分类
	 * @param reqDto
	 * @param token
	 * @return
	 */
	@RequestMapping("/updateapicontenttype")
	@AuthReq
	public RespJson updateApiContentType(@RequestBody ApiContentTypeReqDto reqDto,String token){
		logger.info(" 修改api接口分类"+ JSON.toJSONString(reqDto));
		ApiContentTypeResDto resDto = null;
		try{
			resDto = apiContentTypeService.updateApiContentType(reqDto);
		}catch (PTUnCheckedException e) {
			return RespJson.buildFailureResponse(e.getOutMsg());
		}catch (Exception e) {
			e.printStackTrace();
			return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
		}
		return RespJson.buildSuccessResponse();
	}

	/**
	 * 删除api接口分类
	 * @param reqDto
	 * @param token
	 * @return
	 */
	@RequestMapping("/deleteapicontenttype")
	@AuthReq
	public RespJson deleteApiContentType(@RequestBody ApiContentTypeReqDto reqDto,String token){
		logger.info(" 删除api接口分类"+ JSON.toJSONString(reqDto));
		ApiContentTypeResDto resDto = null;
		try{
			resDto = apiContentTypeService.updateApiContentType(reqDto);
		}catch (PTUnCheckedException e) {
			return RespJson.buildFailureResponse(e.getOutMsg());
		}catch (Exception e) {
			e.printStackTrace();
			return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
		}
		return RespJson.buildSuccessResponse();
	}

}