package org.apimgr.controller;

import com.alibaba.fastjson.JSON;
import org.apimgr.annotation.AuthReq;
import org.apimgr.constants.StringConstants;
import org.apimgr.dto.ApiRequestParamDescReqDto;
import org.apimgr.dto.ApiRequestParamDescResDto;
import org.apimgr.exception.PTUnCheckedException;
import org.apimgr.service.ApiRequestParamDescService;
import org.apimgr.util.RespJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 请求参数描述Controller
 * Created by yangjie on 2017/8/22.
 */
@RestController
@RequestMapping("/requestparamdesc")
public class RequestParamDescController {

    //日志
    private static final Logger logger = LoggerFactory.getLogger(RequestParamDescController.class);

    @Resource
    private ApiRequestParamDescService apiRequestParamDescService;

    /**
     * 多条件查询请求参数描述
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/getapirequestparamdescbyparams")
    @AuthReq
    public RespJson getApiRequestParamDescByParams(@RequestBody ApiRequestParamDescReqDto reqDto,String token){
        logger.info(" 多条件查询请求参数描述"+ JSON.toJSONString(reqDto));
        ApiRequestParamDescResDto resDto = null;
        try{
            resDto = apiRequestParamDescService.getApiRequestParamDescByParams(reqDto);
        }catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        }catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse(resDto);
    }

    /**
     * 添加请求参数描述
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/addapirequestparamdesc")
    @AuthReq
    public RespJson addApiRequestParamDesc(@RequestBody ApiRequestParamDescReqDto reqDto,String token){
        logger.info(" 添加请求参数描述"+ JSON.toJSONString(reqDto));
        ApiRequestParamDescResDto resDto = null;
        try{
            resDto = apiRequestParamDescService.addApiRequestParamDesc(reqDto);
        }catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        }catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }

    /**
     * 修改请求参数描述
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/updateapirequestparamdesc")
    @AuthReq
    public RespJson updateApiRequestParamDesc(@RequestBody ApiRequestParamDescReqDto reqDto,String token){
        logger.info(" 修改请求参数描述"+ JSON.toJSONString(reqDto));
        ApiRequestParamDescResDto resDto = null;
        try{
            resDto = apiRequestParamDescService.updateApiRequestParamDesc(reqDto);
        }catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        }catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }

    /**
     * 删除请求参数描述
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/deleteapirequestparamdesc")
    @AuthReq
    public RespJson deleteApiRequestParamDesc(@RequestBody ApiRequestParamDescReqDto reqDto, String token){
        logger.info(" 删除请求参数描述"+ JSON.toJSONString(reqDto));
        ApiRequestParamDescResDto resDto = null;
        try{
            resDto = apiRequestParamDescService.updateApiRequestParamDesc(reqDto);
        }catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        }catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }

}
