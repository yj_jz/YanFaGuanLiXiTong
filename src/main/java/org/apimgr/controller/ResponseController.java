package org.apimgr.controller;

import com.alibaba.fastjson.JSON;
import org.apimgr.annotation.AuthReq;
import org.apimgr.constants.StringConstants;
import org.apimgr.dto.ApiResponseReqDto;
import org.apimgr.dto.ApiResponseResDto;
import org.apimgr.exception.PTUnCheckedException;
import org.apimgr.service.ApiResponseService;
import org.apimgr.util.RespJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Created by yangjie on 2017/8/18.
 * 响应返回Controller
 */
public class ResponseController {

    //日志
    private static final Logger logger = LoggerFactory.getLogger(ResponseController.class);

    @Resource
    private ApiResponseService apiResponseService;

    /**
     * 多条件查询api响应返回
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/getapiresponsebyparams")
    @AuthReq
    public RespJson getApiResponseByParams(@RequestBody ApiResponseReqDto reqDto,String token){
        logger.info(" 多条件查询api响应返回"+ JSON.toJSONString((reqDto).toString()));
        ApiResponseResDto resDto = null;
        try{
            resDto = apiResponseService.getApiResponseByParam(reqDto);
        }catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        }catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse(resDto);
    }

    /**
     * 添加api响应返回
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/addapiresponse")
    @AuthReq
    public RespJson addApiResponse(@RequestBody ApiResponseReqDto reqDto,String token){
        logger.info(" 添加api响应返回"+ JSON.toJSONString((reqDto).toString()));
        ApiResponseResDto resDto = null;
        try{
            resDto = apiResponseService.addApiResponse(reqDto);
        }catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        }catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }

    /**
     * 修改api响应返回
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/updateapiresponse")
    @AuthReq
    public RespJson updateApiResponse(@RequestBody ApiResponseReqDto reqDto,String token){
        logger.info(" 修改api响应返回"+ JSON.toJSONString((reqDto).toString()));
        ApiResponseResDto resDto = null;
        try{
            resDto = apiResponseService.updateApiResponse(reqDto);
        }catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        }catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }

    /**
     * 删除api响应返回
     * @param reqDto
     * @param token
     * @return
     */
    @RequestMapping("/deleteapiresponse")
    @AuthReq
    public RespJson deleteApiResponse(@RequestBody ApiResponseReqDto reqDto, String token){
        logger.info(" 删除api响应返回"+ JSON.toJSONString(reqDto).toString());
        ApiResponseResDto resDto = null;
        try{
            resDto = apiResponseService.updateApiResponse(reqDto);
        }catch (PTUnCheckedException e) {
            return RespJson.buildFailureResponse(e.getOutMsg());
        }catch (Exception e) {
            e.printStackTrace();
            return RespJson.buildFailureResponse(StringConstants.SYSTEM_ERROR);
        }
        return RespJson.buildSuccessResponse();
    }

}
