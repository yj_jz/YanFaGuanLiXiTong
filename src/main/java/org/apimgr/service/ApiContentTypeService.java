package org.apimgr.service;

import org.apimgr.dto.ApiContentTypeReqDto;
import org.apimgr.dto.ApiContentTypeResDto;
import org.apimgr.entity.ApiContentType;
import org.apimgr.enums.PTExceptionCode;
import org.apimgr.exception.PTUnCheckedException;
import org.apimgr.mapper.ApiContentTypeMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * api接口分类Service 
 * Create by yangjie on 2017/08/24 
*/
@Service
public class ApiContentTypeService{

	@Resource
	private ApiContentTypeMapper iapiContentTypeMapper;

	/**
	 * 多条件查询api接口分类
	 * @param request
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ApiContentTypeResDto getApiContentTypeByParams(ApiContentTypeReqDto request){
		ApiContentTypeResDto resDto= new ApiContentTypeResDto();
		List<ApiContentType> apiContentTypeList = iapiContentTypeMapper.getApiContentTypeByParams(request);
		resDto.setApiContentTypeList(apiContentTypeList);
		return resDto;
	}


	/**
	 * 添加api接口分类
	 * @param request
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ApiContentTypeResDto addApiContentType(ApiContentTypeReqDto request){
		ApiContentTypeResDto resDto= new ApiContentTypeResDto();
		int i = iapiContentTypeMapper.addApiContentType(request);
		if(i == 0){
			throw  new PTUnCheckedException(PTExceptionCode.BD_INSERT_EXPT.getCode());
		}
		return resDto;
	}


	/**
	 * 修改api接口分类
	 * @param request
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ApiContentTypeResDto updateApiContentType(ApiContentTypeReqDto request){
		ApiContentTypeResDto resDto= new ApiContentTypeResDto();
		int i = iapiContentTypeMapper.updateApiContentType(request);
		if(i == 0){
			throw  new PTUnCheckedException(PTExceptionCode.BD_UPDATE_EXPT.getCode());
		}
		return resDto;
	}


	/**
	 * 删除api接口分类
	 * @param request
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ApiContentTypeResDto deleteApiContentType(ApiContentTypeReqDto request){
		ApiContentTypeResDto resDto= new ApiContentTypeResDto();
		int i = iapiContentTypeMapper.deleteApiContentType(request);
		if(i == 0){
			throw  new PTUnCheckedException(PTExceptionCode.BD_DELETE_EXPT.getCode());
		}
		return resDto;
	}

}