package org.apimgr.service;

import org.apimgr.dto.ApiResponseReqDto;
import org.apimgr.dto.ApiResponseResDto;
import org.apimgr.entity.ApiResponse;
import org.apimgr.enums.PTExceptionCode;
import org.apimgr.exception.PTUnCheckedException;
import org.apimgr.mapper.ApiResponseMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by yangjie on 2017/8/1.
 *API响应Service
 */
@Service
public class ApiResponseService {
    
    @Resource
    private ApiResponseMapper apiResponseMapper;

    /**
     * Api响应信息的多条件查询
     * @param reqDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public ApiResponseResDto getApiResponseByParam(ApiResponseReqDto reqDto){
        ApiResponseResDto resDto  = new ApiResponseResDto();
        List<ApiResponse> apiResList = apiResponseMapper.getApiResponseByParams(reqDto);
        resDto.setApiResponseList(apiResList);
        return  resDto;
    }

    /**
     * 添加Api响应信息
     * @param reqDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public ApiResponseResDto addApiResponse(ApiResponseReqDto reqDto){
        ApiResponseResDto resDto  = new ApiResponseResDto();
        try {
            int i = apiResponseMapper.addApiResponse(reqDto);
            if(i == 0){
                throw new PTUnCheckedException(PTExceptionCode.BD_INSERT_EXPT.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PTUnCheckedException(PTExceptionCode.BD_INSERT_EXPT.getCode());
        }
        return  resDto;
    }


    /**
     * 修改Api响应信息
     * @param reqDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public  ApiResponseResDto updateApiResponse(ApiResponseReqDto reqDto){
        ApiResponseResDto resDto  = new ApiResponseResDto();
        try {
            int i = apiResponseMapper.updateApiResponse(reqDto);
            if(i == 0){
                throw new PTUnCheckedException(PTExceptionCode.BD_UPDATE_EXPT.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PTUnCheckedException(PTExceptionCode.BD_UPDATE_EXPT.getCode());
        }
        return  resDto;
    }


    /**
     * 删除Api响应信息
     * @param reqDto
     * @return
     */
    public  ApiResponseResDto deleteApiResponse(ApiResponseReqDto reqDto){
        ApiResponseResDto resDto  = new ApiResponseResDto();
        try {
            int i = apiResponseMapper.deleteApiResponse(reqDto);
            if(i == 0){
                throw new PTUnCheckedException(PTExceptionCode.BD_DELETE_EXPT.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PTUnCheckedException(PTExceptionCode.BD_DELETE_EXPT.getCode());
        }
        return  resDto;
    }
    
    
}
