package org.apimgr.service;

import org.apimgr.dto.ApiContentReqDto;
import org.apimgr.dto.ApiContentResDto;
import org.apimgr.dto.ApiRequestParamDescReqDto;
import org.apimgr.dto.ApiResponseReqDto;
import org.apimgr.entity.ApiContent;
import org.apimgr.entity.ApiResponse;
import org.apimgr.enums.PTExceptionCode;
import org.apimgr.exception.PTUnCheckedException;
import org.apimgr.mapper.ApiContentMapper;
import org.apimgr.mapper.ApiRequestParamDescMapper;
import org.apimgr.mapper.ApiResponseDescMapper;
import org.apimgr.mapper.ApiResponseMapper;
import org.apimgr.util.ConstantsUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yangjie on 2017/8/1.
 * API用户Service
 */
@Service
public class ApiContentService {


    @Resource
    private ApiContentMapper apiContentMapper;

    @Resource
    private ApiResponseMapper apiResponseMapper;

    @Resource
    private ApiResponseDescMapper apiResponseDescMapper;

    @Resource
    private ApiRequestParamDescMapper apiRequestParamDescMapper;

    /**
     * 多条件查询API内容信息
     * @param reqDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public ApiContentResDto getApiContentByParam(ApiContentReqDto reqDto){
        ApiContentResDto  resDto = new ApiContentResDto();
        List<ApiContent> apiContentList = apiContentMapper.getApiContentByParams(reqDto);
        resDto.setApiContentList(apiContentList);
        return  resDto;
    }


    /**
     * 添加API内容信息
     * @param reqDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public  ApiContentResDto addApiContent(ApiContentReqDto reqDto){
        ApiContentResDto resDto = new ApiContentResDto();
        int i = 0;
        try {
            i = apiContentMapper.addApiContent(reqDto);

            //添加请求参数描述
            i = apiRequestParamDescMapper.batchAddApiRequestParamDesc(reqDto.getId(),reqDto.getApiRequestParamDescList());

            //添加正确响应和错误响应

            ApiResponseReqDto  apiResponseReqDto = new ApiResponseReqDto(reqDto.getWrongJson(),ConstantsUtil.WRONG_RESPONSE_CODE,reqDto.getId());
            apiResponseReqDto.setWrong_enum(reqDto.getWrongEnum());
            i = apiResponseMapper.addApiResponse(apiResponseReqDto);


            apiResponseReqDto = new ApiResponseReqDto(reqDto.getRightJson(),ConstantsUtil.RIGHT_RESPONSE_CODE,reqDto.getId());
            i = apiResponseMapper.addApiResponse(apiResponseReqDto);



            //批量添加响应描述
            i = apiResponseDescMapper.batchAddApiResponseDesc(reqDto.getApiResponses(),apiResponseReqDto.getId());


        } catch (Exception e) {
            e.printStackTrace();
            throw new PTUnCheckedException(PTExceptionCode.BD_INSERT_EXPT.getCode());
        }
        return  resDto;
    }


    /**
     * 修改API内容信息
     * @param reqDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public  ApiContentResDto updateApiContent(ApiContentReqDto reqDto){
        ApiContentResDto resDto = new ApiContentResDto();
        try {
            int i = apiContentMapper.updateApiContent(reqDto);
            if(i == 0){
                throw new PTUnCheckedException(PTExceptionCode.BD_UPDATE_EXPT.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PTUnCheckedException(PTExceptionCode.BD_UPDATE_EXPT.getCode());
        }
        return  resDto;
    }

    /**
     * 删除API内容信息
     * @param reqDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public  ApiContentResDto deleteApiContent(ApiContentReqDto reqDto){
        ApiContentResDto resDto = new ApiContentResDto();
        try {
            //根据content_id删除请求描述
            ApiRequestParamDescReqDto apiRequestParamDescReqDto = new ApiRequestParamDescReqDto();
            apiRequestParamDescReqDto.setContent_id(reqDto.getId());
           int i = apiRequestParamDescMapper.deleteApiRequestParamDesc(apiRequestParamDescReqDto);

            //根据content_id删除响应结果
            ApiResponseReqDto apiResponseReqDto = new ApiResponseReqDto();
            apiResponseReqDto.setApi_content_id(reqDto.getId());
            i = apiResponseMapper.deleteApiResponse(apiResponseReqDto);

            //根据repsonse_id查询对应的响应并删除
            List<ApiResponse> responseList = apiResponseMapper.getApiResponseByParams(apiResponseReqDto);
            List<Integer> responseIdList = new ArrayList<Integer>();
            for (ApiResponse apiResponse : responseList) {
                responseIdList.add(apiResponse.getId());
            }
            i = apiResponseDescMapper.deleteResponseDescByIds(responseIdList);

            //删除接口
            i = apiContentMapper.deleteApiContent(reqDto);
            if(i == 0){
                throw new PTUnCheckedException(PTExceptionCode.BD_DELETE_EXPT.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new PTUnCheckedException(PTExceptionCode.BD_DELETE_EXPT.getCode());
        }
        return  resDto;
    }






}
