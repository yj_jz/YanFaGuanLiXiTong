package org.apimgr.service;

import org.apimgr.dto.ApiRequestParamDescReqDto;
import org.apimgr.dto.ApiRequestParamDescResDto;
import org.apimgr.entity.ApiRequestParamDesc;
import org.apimgr.enums.PTExceptionCode;
import org.apimgr.exception.PTUnCheckedException;
import org.apimgr.mapper.ApiRequestParamDescMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 请求参数描述Service 
 * Create by yangjie on 2017/08/22 
*/
@Service
public class ApiRequestParamDescService{

	@Resource
	private ApiRequestParamDescMapper iapiRequestParamDescMapper;

	/**
	 * 多条件查询请求参数描述
	 * @param request
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public  ApiRequestParamDescResDto getApiRequestParamDescByParams(ApiRequestParamDescReqDto request){
		ApiRequestParamDescResDto resDto= new ApiRequestParamDescResDto();
		List<ApiRequestParamDesc> apiRequestParamDescList = iapiRequestParamDescMapper.getApiRequestParamDescByParams(request);
		resDto.setApiRequestParamDescList(apiRequestParamDescList);
		return resDto;
	}


	/**
	 * 添加请求参数描述
	 * @param request
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ApiRequestParamDescResDto addApiRequestParamDesc(ApiRequestParamDescReqDto request){
		ApiRequestParamDescResDto resDto= new ApiRequestParamDescResDto();
		int i = iapiRequestParamDescMapper.addApiRequestParamDesc(request);
		if(i == 0){
			throw  new PTUnCheckedException(PTExceptionCode.BD_INSERT_EXPT.getCode());
		}
		return resDto;
	}


	/**
	 * 修改请求参数描述
	 * @param request
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ApiRequestParamDescResDto updateApiRequestParamDesc(ApiRequestParamDescReqDto request){
		ApiRequestParamDescResDto resDto= new ApiRequestParamDescResDto();
		int i = iapiRequestParamDescMapper.updateApiRequestParamDesc(request);
		if(i == 0){
			throw  new PTUnCheckedException(PTExceptionCode.BD_UPDATE_EXPT.getCode());
		}
		return resDto;
	}


	/**
	 * 删除请求参数描述
	 * @param request
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public ApiRequestParamDescResDto deleteApiRequestParamDesc(ApiRequestParamDescReqDto request){
		ApiRequestParamDescResDto resDto= new ApiRequestParamDescResDto();
		int i = iapiRequestParamDescMapper.deleteApiRequestParamDesc(request);
		if(i == 0){
			throw  new PTUnCheckedException(PTExceptionCode.BD_DELETE_EXPT.getCode());
		}
		return resDto;
	}

}