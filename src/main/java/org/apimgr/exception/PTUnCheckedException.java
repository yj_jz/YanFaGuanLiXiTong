package org.apimgr.exception;

import org.apimgr.enums.PTExceptionCode;

/**
 * Created by yangjie on 2017/8/2.
 * 集成运行时异常
 */
public class PTUnCheckedException extends  RuntimeException {

    private static final long serialVersionUID = 7471328974250045016L;


    private int resultCode;

    private String code;

    public String getOutMsg()
    {
        if (code.equals("00"))
        {
            return getMessage();
        }
        PTExceptionCode mcode = PTExceptionCode.getByCode(code);
        if (mcode != null)
        {
            return mcode.getDesout();
        }
        return getMessage();
    }

    public String getInMsg()
    {
        PTExceptionCode mcode = PTExceptionCode.getByCode(code);
        if (mcode != null)
        {
            return mcode.getDesin();
        }
        return getMessage();
    }

    public PTUnCheckedException()
    {
        super();
    }

    public PTUnCheckedException(Throwable e)
    {
        super(e);
    }

    public PTUnCheckedException(String errorCode)
    {
        super();
        this.code = errorCode;
    }

    public PTUnCheckedException(int resultCode,String errorCode)
    {
        super();
        this.code = errorCode;
        this.resultCode = resultCode;
    }

    public PTUnCheckedException(PTExceptionCode mcode, Throwable e)
    {
        super(mcode.getDesin(), e);
        this.code = mcode.getCode();
    }

    public PTUnCheckedException(String code, Throwable e)
    {
        super(PTExceptionCode.getInMsg(code), e);
        this.code = code;
    }

    public PTUnCheckedException(String code, String message)
    {
        super(message);
        this.code = code;
    }

    public PTUnCheckedException(String code, String message, Throwable e)
    {
        super(message, e);
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }


    public int getResultCode() {
        return resultCode;
    }
}
