package org.apimgr.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apimgr.dto.common.BaseResDto;
import org.apimgr.entity.ApiRequestParamDesc;

import java.util.List;

/**
 * Created by yangjie on 2017/8/22.
 *请求参数描述ResDto
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiRequestParamDescResDto extends BaseResDto {

    /**
     * 请求参数描述集合
     */
    private List<ApiRequestParamDesc> apiRequestParamDescList;

    public List<ApiRequestParamDesc> getApiRequestParamDescList() {
        return apiRequestParamDescList;
    }

    public void setApiRequestParamDescList(List<ApiRequestParamDesc> apiRequestParamDescList) {
        this.apiRequestParamDescList = apiRequestParamDescList;
    }
}
