package org.apimgr.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apimgr.dto.common.BaseReqDto;

/**
 * Created by yangjie on 2017/8/1.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiUserReqDto extends BaseReqDto {

    private Integer id;

    private String username;

    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

}
