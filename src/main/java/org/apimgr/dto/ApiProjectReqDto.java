package org.apimgr.dto;

import org.apimgr.dto.common.BaseReqDto;

/**
 * Created by yangjie on 2017/8/2.
 */
public class ApiProjectReqDto extends BaseReqDto {

    /**
     *项目编号
     */
    private Integer id;


    /**
     * 项目名称
     */
    private String project_name;


    /**
     * 用户编号
     */
    private Integer user_id;


    public Integer getId(){
        return id;
    }


    public void setId( Integer id){
        this.id=id;
    }


    public String getProject_name(){
        return project_name;
    }


    public void setProject_name( String project_name){
        this.project_name=project_name;
    }


    public Integer getUser_id(){
        return user_id;
    }


    public void setUser_id( Integer user_id){
        this.user_id=user_id;
    }


}