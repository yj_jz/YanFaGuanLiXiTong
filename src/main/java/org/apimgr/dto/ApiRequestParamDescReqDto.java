package org.apimgr.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apimgr.dto.common.BaseReqDto;

/**
 * Created by yangjie on 2017/8/22.
 * 请求参数描述ReqDto
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiRequestParamDescReqDto  extends BaseReqDto{

    /**
     * 请求参数描述编号
     */
    private Integer id;


    /**
     * 请求参数名称
     */
    private String request_param_name;


    /**
     * 是否必填
     */
    private Boolean is_required;


    /**
     * 数据类型
     */
    private String data_type;


    /**
     * 参数描述
     */
    private String req_param_desc;


    /**
     * 所属接口编号
     */
    private Integer content_id;


    public Integer getId(){
        return id;
    }


    public void setId( Integer id){
        this.id=id;
    }


    public String getRequest_param_name(){
        return request_param_name;
    }


    public void setRequest_param_name( String request_param_name){
        this.request_param_name=request_param_name;
    }


    public Boolean getIs_required(){
        return is_required;
    }


    public void setIs_required( Boolean is_required){
        this.is_required=is_required;
    }


    public String getData_type(){
        return data_type;
    }


    public void setData_type( String data_type){
        this.data_type=data_type;
    }


    public String getReq_param_desc(){
        return req_param_desc;
    }


    public void setReq_param_desc( String req_param_desc){
        this.req_param_desc=req_param_desc;
    }


    public Integer getContent_id(){
        return content_id;
    }


    public void setContent_id( Integer content_id){
        this.content_id=content_id;
    }



}
