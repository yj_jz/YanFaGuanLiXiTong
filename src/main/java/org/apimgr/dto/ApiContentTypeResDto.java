package org.apimgr.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apimgr.dto.common.BaseResDto;
import org.apimgr.entity.ApiContentType;

import java.util.List;

/**
 * Created by yangjie on 2017/8/24.
 * api接口分类ResDto
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiContentTypeResDto extends BaseResDto {


    /**
     * API接口分类集合
     */
    private List<ApiContentType> apiContentTypeList;

    public List<ApiContentType> getApiContentTypeList() {
        return apiContentTypeList;
    }

    public void setApiContentTypeList(List<ApiContentType> apiContentTypeList) {
        this.apiContentTypeList = apiContentTypeList;
    }
}
