package org.apimgr.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apimgr.dto.common.BaseResDto;
import org.apimgr.entity.ApiContent;

import java.util.List;

/**
 * Created by yangjie on 2017/8/3.
 * Api内容响应Dto
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiContentResDto extends BaseResDto {

    private List<ApiContent> apiContentList;

    public List<ApiContent> getApiContentList() {
        return apiContentList;
    }

    public void setApiContentList(List<ApiContent> apiContentList) {
        this.apiContentList = apiContentList;
    }
}
