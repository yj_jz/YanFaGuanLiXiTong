package org.apimgr.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apimgr.dto.common.BaseResDto;
import org.apimgr.entity.ApiUser;

/**
 * Created by yangjie on 2017/8/1.
 * 用户响应类
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiUserResDto extends BaseResDto {

    /**
     * 用户对象
     */
    private ApiUser apiUser;

    /**
     * token鉴权
     */
    private  String token;

    public ApiUser getApiUser() {
        return apiUser;
    }

    public void setApiUser(ApiUser apiUser) {
        this.apiUser = apiUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
