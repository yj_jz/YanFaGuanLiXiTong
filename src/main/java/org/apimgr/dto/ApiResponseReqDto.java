package org.apimgr.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apimgr.dto.common.BaseReqDto;
import org.apimgr.entity.ApiResponse;

import java.util.List;

/**
 * Created by yangjie on 2017/8/3.
 *  Api响应结果 ResDto
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponseReqDto extends BaseReqDto {


    /**
     *
     */
    private Integer id;


    /**
     * 响应的内容
     */
    private String response_content;


    /**
     * 响应的类型(1.正确响应   2.错误响应)
     */
    private Integer response_type;


    /**
     * api接口内容编号
     */
    private Integer api_content_id;

    /**
     * 错误信息的枚举
     */
    private String wrong_enum;

    /**
     * API响应结果集合
     */
    private List<ApiResponse> apiResponses;

    public ApiResponseReqDto() {
    }

    public ApiResponseReqDto(String response_content, Integer response_type, Integer api_content_id) {
        this.response_content = response_content;
        this.response_type = response_type;
        this.api_content_id = api_content_id;
    }

    public Integer getId(){
        return id;
    }


    public void setId( Integer id){
        this.id=id;
    }


    public String getResponse_content(){
        return response_content;
    }


    public void setResponse_content( String response_content){
        this.response_content=response_content;
    }


    public Integer getResponse_type(){
        return response_type;
    }


    public void setResponse_type( Integer response_type){
        this.response_type=response_type;
    }


    public Integer getApi_content_id(){
        return api_content_id;
    }


    public void setApi_content_id( Integer api_content_id){
        this.api_content_id=api_content_id;
    }

    public List<ApiResponse> getApiResponses() {
        return apiResponses;
    }

    public void setApiResponses(List<ApiResponse> apiResponses) {
        this.apiResponses = apiResponses;
    }

    public String getWrong_enum() {
        return wrong_enum;
    }

    public void setWrong_enum(String wrong_enum) {
        this.wrong_enum = wrong_enum;
    }


}
