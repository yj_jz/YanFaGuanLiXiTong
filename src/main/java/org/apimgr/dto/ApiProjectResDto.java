package org.apimgr.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apimgr.dto.common.BaseResDto;
import org.apimgr.entity.ApiProject;

import java.util.List;

/**
 * Created by yangjie on 2017/8/2.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiProjectResDto extends BaseResDto {


    /**
     * API项目列表
     */
    private List<ApiProject> apiProjectList;

    public List<ApiProject> getApiProjectList() {
        return apiProjectList;
    }

    public void setApiProjectList(List<ApiProject> apiProjectList) {
        this.apiProjectList = apiProjectList;
    }
}
