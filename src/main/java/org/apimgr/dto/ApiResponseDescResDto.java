package org.apimgr.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apimgr.dto.common.BaseResDto;
import org.apimgr.entity.ApiResponseDesc;

import java.util.List;

/**
 * Created by yangjie on 2017/9/6.
 * API响应描述ResDto
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponseDescResDto extends BaseResDto{


    private List<ApiResponseDesc> apiResponseDescs;

    public List<ApiResponseDesc> getApiResponseDescs() {
        return apiResponseDescs;
    }

    public void setApiResponseDescs(List<ApiResponseDesc> apiResponseDescs) {
        this.apiResponseDescs = apiResponseDescs;
    }
}
