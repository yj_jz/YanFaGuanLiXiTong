package org.apimgr.dto.common;

import java.io.Serializable;

/**
 * Created by yangjie on 2017/8/1.
 */
public class BaseResDto implements Serializable {


    private static final long serialVersionUID = -7269932769018116108L;

    /**
     * 返回状态 0失败 1成功
     */
    protected String resultCode = String.valueOf("成功");

    /**
     * 返回给调用者的消息
     */
    protected String resultMsg = "";
}
